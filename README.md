# License2Chill

## Behavior Driven Development

Each test consist of common parts for each test case:

- **Given** -  create initial state for the test _(optional)_.
- **When** - let the system under test do _the thing_.
- **Then** -  assert for the correct system response.

For environment sharing between those methods, **State** is
defined. It usually contains system under test (sut) as well as sut's
outputs for later investigation in *then* functions. It's up to the
tester (you) how it will be used. Then it's passed between the bdd
testing functions.

## Usage

Let's firstly define some system to test.

```swift
protocol Operation {
    func evaluate(x: Int, y: Int) throws -> Int
}

struct Plus: Operation {
    func evaluate(x: Int, y: Int) throws -> Int {
        return x + y
    }
}
```

Then, the tests file could look like this:

```swift
import Testing
import License2Chill

class MathOperationsTests: BddSuite {
    private var state: State

    init() {
        state = State()
    }

    @Test func plus_CalculatesZero() throws {
        given.operation(&state, Plus())
        try when.evaluate(&state, 0, 0)
        try then.expectResult(state, 0)
    }

    @Test func plus_SimpleComputation() throws {
        given.operation(&state, Plus())
        try when.evaluate(&state, 1, 2)
        try then.expectResult(state, 3)
    }
}

private struct State {
    // System Under Test
    var sut: Operation!
    var result: Int!
}

private extension Given {
    func operation(_ state: inout State, _ operation: Operation) {
        state.sut = operation
    }
}

private extension When {
    func evaluate(_ state: inout State, _ x: Int, _ y: Int) throws {
        state.result = try state.sut?.evaluate(x: x, y: y)
    }
}

private extension Then {
    func expectResult(_ state: State, _ result: Int) throws {
        #expect(state.result == result)
    }
}
```
