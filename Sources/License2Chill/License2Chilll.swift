import Foundation

public protocol BddSuite { }

public extension BddSuite {
    var given: Given { Given() }
    var when: When { When() }
    var then: Then { Then() }
}

public struct Given {}

public struct When {}

public struct Then {}
